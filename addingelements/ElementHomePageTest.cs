﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

[TestFixture]
public class ElementHomePageTest : WebPage
    {
        IWebDriver WebDriver = null;

        [SetUp]
        public void SettingUpURl_LaunchBrowser()
        {
            WebDriver = LaunchBrowser("chrome");
            WebDriver.Manage().Window.Maximize();
            WebDriver.Navigate().GoToUrl("https://the-internet.herokuapp.com/add_remove_elements/");
        }

        [Test]
        public void ValidateAddedElements()
        {
            var ElementHomePage = new ElementHomePage();
            int count = ElementHomePage.GetCountOfAddedElements(10);
            Assert.AreEqual(count, 10);
        }

        [TearDown]
         public void CloseBrowser()
        {
        WebDriver.Quit();
        }
    }


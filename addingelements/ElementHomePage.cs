﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;


    public class ElementHomePage : WebPage
    {
        
        public int GetCountOfAddedElements(int countTobeAdded)
        {
            if (countTobeAdded > 0)
            {
                try
                {
                    IWebElement AddButton = WebDriver.FindElement(By.XPath("//button[text() = 'Add Element']"));
                    if(AddButton.Enabled && AddButton.Displayed) { 
                        for (int i =0; i<countTobeAdded; i++)
                        {
                            AddButton.Click();  
                        }
                        return WebDriver.FindElements(By.XPath("//button[text() = 'Delete']")).Count;
                    }
                }
                catch(NoSuchElementException ex)
                {
                    throw new NoSuchElementException("Add Element Button is not available to add Elements");
                }
            }
            return 0;
                
        }

    }



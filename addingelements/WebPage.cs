﻿using System;
using System.Reflection;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using NUnit.Framework;

public class WebPage
{

    public static IWebDriver WebDriver;

  
    public IWebDriver LaunchBrowser(String BrowserName)
    {
        switch (BrowserName)
        {
        //Driver path must be relative path
            case "chrome":
                WebDriver = new ChromeDriver(@"C:\Automation Batches\DotnetProject\invoicecloud\AutomationTesting\addingelements\addingelements\");
                break;

            case "firefox":
                WebDriver = new FirefoxDriver("FireFox Driver Path");
                break;

            default:
                WebDriver = null;
                break;
        }
        return WebDriver;
    }

}